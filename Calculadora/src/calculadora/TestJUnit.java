package calculadora;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

 public class TestJUnit {

	@Test
	void test() {
		assertEquals(3,Principal.testDePrueba(3));//primer metodo
		
		
		if(Principal.testDePruebaAleatorio(2, 10)>2&& Principal.testDePruebaAleatorio(2, 10)<10){
			assertTrue("Funciona bien", true);
		}
		else{
			assertFalse(false,"Funciona mal");
		}//segundo metodo
		
		
		assertEquals("Este numero es par",Principal.testDePruebaCadena(2));//tercer metodo
		assertEquals("Este numero es impar",Principal.testDePruebaCadena(1));
	}
	
}
